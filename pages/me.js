import React from 'react'
import Link from 'next/link'
import Feature from '../components/feature'
import Head from '../components/head'

const Home = () => (
  <div className="content">
    <Head title="Me" />
    <div className="header-container">
      <img className="logo" src="./static/logo.svg" />
      <nav className="menu">
      <li>What you can do</li>
      <li>Templates</li>
      {/* <li className="menu-item">Pricing</li> */}
      <li><a className="button--small">Get Beta Access</a></li>
      </nav>
    </div>
    <div className="hero">
    <div className="heroContainer">
      <div className="heroText">

        <h1><strong className="h1-highlight">Build forms</strong> and Capture Hot Leads.</h1>
        <p>
        Design, collect and report your leads all in one place with Pepperform.
        </p>
        {/* <button className="secondary">Joigramme</button> */}
        <button>Get BETA access</button>
      </div>
      <div>
        <img className="HeroImage" src="./static/capture.svg" width="535px" />
      </div>
      </div>
    </div>
    
    <div className="bump">
      <img src="./static/bump.svg" />
    </div>
    <div className="features">
      
    </div>
    <div className="information">
      <div className="information__content">
      <div>
        <h3>Powerful customisation features 👊</h3>
      </div>
      <div className="features__list">
      <Feature title="blah" image="blah"></Feature>
      <Feature></Feature>
      <Feature></Feature>
      <Feature></Feature>
      <Feature></Feature>
      <Feature></Feature>
      </div>
      </div>
    </div>
    <div className="information">
      <div className="information__content">
        <h2>How does this work?</h2>
        <p>abc</p>
      </div>
    </div>
    <div className="footer">
    <div className="footer__content">
        <p>Copyright Pepperform.co</p>
      </div>
    </div>
    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: Proxima Nova, -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
        color: #9A9DAB;
        font-size:16px;
      }
      .content {
        // background: url('./static/Oval.svg') no-repeat;
        overflow:hidden;
        background-position: 140% 80px;
      }
      button {
        color: #fff;
        background: #2E3696;
        border: 0;
        font-size:16px;
        padding: 16px 28px;
        font-weight:600;
        border-radius:8px;
        width:100%;
      }
      .button--small {
        color: #2068F6;
        // background: #2E3696;
        border: 1px solid rgba(154, 157, 171, 0.3);
        font-size:14px;
        padding: 8px 16px;
        font-weight:600;
        border-radius:8px;
        width:100%;
      }
      @media only screen and (min-width: 768px){
        button {
          color: #fff;
          background: #2E3696;
          width:auto;
          border: 0;
          font-size:16px;
          padding: 16px 28px;
          font-weight:600;
          border-radius:8px;
        }
      }
      .secondary {
        color: #2E3696;
        background: #fff;
        border: 1px solid #D4D7D9;
        margin-right: 20px;
        font-size:16px;
        font-weight:600;
        padding: 16px 28px;
        // border-radius:8px;
        border-radius:8px;
      }
      h1, h2, h3 {
        font-weight:600;
      }
      .highlight {
        background: #2068F6;
        color: #fff;
        font-size: 10px;
        font-weight: bold;
        text-transform: uppercase;
        padding: 4px 8px;
        margin: 0 4px;
        border-radius: 4px;
      }
      h1 {
        font-size:50px;
        line-height:1;
        margin:0;
        color:#3A427E;
        margin-bottom:20px;
      }
      strong.h1-highlight{
        color:#2068F6;
      }
      h2 {

      }
      h3 {
        font-weight:600;
        font-size:24px;
        text-align:center;
        color: #3A427E;
      }
      p {
        margin: 0;
        margin-bottom:28px;
      }
      .logo{
        padding:0;
      }
      .HeroImage {
        overflow:hidden;
        z-index:1;
        width:480px;
      }
      .heroText {
        padding-top:24px;
      }
      @media only screen and (min-width: 768px){
        .heroText {
          max-width:500px;
          padding-top:64px;
        }
      }
      .hero {
        margin: auto;
        padding: 52px 0 0;
        font-size:20px;
        line-height: 1.6;
        overflow:hidden;
      }
      .heroContainer{
        margin:auto;
        padding:0 28px; 
      }
      @media only screen and (min-width: 768px){
        .heroContainer{
        max-width:1000px;
        display: grid;
        padding:0 48px; 
        grid-template-columns: 60% 40%;
        }
      }
      .header-container{
        grid-template-columns: 80px 50%;
        margin:auto;
        padding:16px 32px;
      }
      @media only screen and (min-width: 768px){
        .header-container{
          max-width:1000px;
          display: grid;
          grid-template-columns: 50% 50%;
          margin:auto;
          padding:32px 0;
        }
      }
      nav {
        grid-template-columns: 10% 10% 50%;
        text-align: right;
      }
      nav li{
        display:inline-block;
        padding:8px 0 0 20px;
      }
      .bump img {
        // background: url("./static/bump.svg") no-repeat;
        background-size:cover;
        display:block;
        width:100%;
        min-height:100px;
      }
      .features {
        z-index:2;
        background: #247FDB;
        width: 100%;
        min-height:300px;
        padding: 56px 0; 
      }
      .features__content {
        max-width: 800px;       
        margin: auto;
      }
      .features__list {
        grid-template-columns: 1fr 1fr 1fr 1fr;
        display: grid;
      }
      .information {      
        border-bottom:1px solid #E6E7E7;
      }
      .information__content {
        max-width: 800px;  
        padding: 80px 0;     
        margin: auto;
      }
    `}</style>
  </div>
)

export default Home
